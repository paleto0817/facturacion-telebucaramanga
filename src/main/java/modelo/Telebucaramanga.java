package modelo;

import java.awt.List;
import java.util.ArrayList;
import javax.swing.JLabel;

public class Telebucaramanga {

    String nombreUsuario;
   Integer cargoBasico;

    public Telebucaramanga(String nombreUsuario, Integer cargoBasico) {
        this.nombreUsuario = nombreUsuario;
        this.cargoBasico = cargoBasico;
    }

    public ArrayList<Integer> valorConsumo(Integer vlrExtraI, Integer MinExtraNac, Integer MinExtraInt ) {

        

        ArrayList<Integer> listaVlr = new ArrayList<>();
            
        Integer vlrExtraN = 0;
        Integer vlrExtI = 0;
       // if (MinExtraNac > 0) {
            vlrExtraN = (MinExtraNac * (vlrExtraI * 1/3));  
            
        //}

        //if (MinExtraInt > 0) {
            vlrExtI = ( MinExtraInt * vlrExtraI);

        //}

        listaVlr.add(vlrExtraN);
        listaVlr.add(vlrExtI);
        return listaVlr;
    }

    public float  vlrIva(Integer cargoBasico, Integer vlrExtraN, Integer vlrExtI) {
        float vlrI = 0;
        vlrI = (float) ((cargoBasico * 0.16) + (vlrExtraN * 0.16) + (vlrExtI * 0.16));
        return vlrI;
    }

    public void Factura(Integer cargoBasico,Integer vlrExtraI, Integer vlrExtraN,Integer vlrExtI, float vlrI, JLabel lblvlrNa, JLabel lblvlrI, JLabel lblvlrIva, JLabel lblvlrTotal) {

        lblvlrNa.setText(""+ vlrExtraN);
        lblvlrI.setText("" + vlrExtI);
        lblvlrIva.setText("" + vlrI);
        lblvlrTotal.setText("" + (vlrExtraN + vlrExtI + vlrI+ cargoBasico));
    }

}
